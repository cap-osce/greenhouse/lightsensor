#include <Arduino.h>
#include <Wire.h>
#include "SSD1306Wire.h"
#include <OneWire.h>
#include "AWSParameters.h"
#include "IOTGateway.h"
#include "StandardConfig.h"
#include "Sensor.h"
#include <NTPClient.h>
#include <WiFiUdp.h>

SSD1306Wire  display(0x3c, 5, 4);

const int sensorPin = 33;

IOTGateway iotGateway(&capgeminiHackathon);
Sensor sensor("light", "light", 2000, 2000);

// Set time
long lastTime = 0;
// Configure message
char msg[512];

WiFiUDP ntpUDP;

NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 0, 60000);

char tempBuffer [10];

void displayLightIntensity(int value) {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 0, "Light");
  display.setFont(ArialMT_Plain_16);

  sprintf (tempBuffer, "%i %%", value);
  display.drawString(65, 0, tempBuffer);
  display.display();
}


void setup() {
  pinMode(sensorPin, INPUT);

    Serial.begin(115200);
    display.init();
    display.flipScreenVertically();
    display.clear();
    // display.setFont(ArialMT_Plain_10);
    iotGateway.initialise();
    delay(2500);
    timeClient.begin();
    timeClient.update();
    Serial.println("Setup complete");
}

void publishLightDetails(int lightValue) {
    char fullTimestamp [25];
    sprintf (fullTimestamp, "%ld", timeClient.getEpochTime());
    lastTime = millis();
    sensor.formatData(msg, 512, lightValue, "light", fullTimestamp);
    iotGateway.publish("measurement/light", msg);
}

void loop() {
  // put your main code here, to run repeatedly:

  int lightValue = 0;
  lightValue = analogRead(sensorPin);
  Serial.println(lightValue);


  int lightValueAsPercentage = map(lightValue, 0, 4095, 100, 0);

  displayLightIntensity(lightValueAsPercentage);
  Serial.println(lightValueAsPercentage);
  if (millis() - lastTime > 2000) {
    publishLightDetails(lightValueAsPercentage);
  }
  delay(1000);
}
