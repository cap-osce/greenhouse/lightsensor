# LightSensor

ESP32 code to read the value of light intensity

The module is 3.3v compatible

The pinout for the module (when facing towards you) is Sense, VCC, GND

The Value of the light sensor is dependent on the max resolution of the adc,
so in the ESP32 case is 4095 (no light detected at all). The mininum value
of the light sensor is 0 and represents the maximum level of light detected.

The light sensor is not linear. 
